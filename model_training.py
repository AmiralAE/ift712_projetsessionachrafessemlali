import numpy as np
import os
import matplotlib.pyplot as plt 
import sklearn
import pandas as pd
from sklearn.preprocessing import LabelBinarizer
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.cluster import KMeans
from sklearn import neighbors
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis


class SupervisedClassifiers:
    def __init__(self, method):
        """
        this class implements methods for initializing the clasifiers and hyperparameters dictionnary 
        and implements  train and test methods for training and testing the classifiers
        Arguement : method, takes value in ['SVM','LDA','DecisionTree','RandomForest','MLP','KNN']
        """
        self.method = method

        #returns the classifier and hyperparameters dictionnary of MLP classifier
        if self.method == 'MLP':
            self.classifier_model = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(100, 99), random_state=1)
            self.parameters = {'hidden_layer_sizes':list(range(20,200)), 'alpha':np.linspace(0.0001, 0.1, 10), 'solver' : ['lbfgs', 'sgd', 'adam'], 'momentum':np.linspace(0.1, 0.9, 10)}
        
        #returns the classifier and hyperparameters dictionnary of RandomForest classifier
        elif self.method == 'RandomForest':
            self.classifier_model = RandomForestClassifier(n_estimators=4, criterion='gini')
            self.parameters = {'max_depth':list(range(1,100)), 'n_estimators':list(range(2,50)), 'random_state':[0, None], 'criterion': 
            ['gini', 'entropy']}
        
        #returns the classifier and hyperparameters dictionnary of DecisionTree classifier
        elif self.method == 'DecisionTree':
            self.classifier_model = DecisionTreeClassifier()
            self.parameters = {'criterion': ['gini', 'entropy'], 'max_features': [None, 'sqrt', 'log2'], 'max_depth':list(range(5,101))}
        
        #returns the classifier and hyperparameters dictionnary of KNN classifier        
        elif self.method == 'KNN':
            self.classifier_model =  KNeighborsClassifier(n_neighbors=20, weights ='distance')
            self.parameters = {'n_neighbors':list(range(5,51)), 'weights':['uniform', 'distance'], 'algorithm':['auto', 'ball_tree', 'kd_tree', 
            'brute']}
                
        #returns the classifier and hyperparameters dictionnary of SVM classifier
        elif self.method == 'SVM':
            self.classifier_model = SVC(gamma='auto')
            self.parameters = {'kernel': ['poly', 'rbf', 'linear', 'sigmoid'], 'C':np.linspace(0.1, 2, 30)}
        
        #returns the classifier and hyperparameters dictionnary of LDA classifier
        elif self.method == 'LDA':
            self.classifier_model = LinearDiscriminantAnalysis(solver='svd')
            self.parameters = {'solver': ['svd', 'eigen', 'lsqr'], 'tol': np.linspace(0.00001,0.0001, 10)}


    def train_classifier(self, x_train, y_train, tuning=True, search='random'): 
        """
        this function returns the best trained classifier of a given method above and the corresponding training accuracy or score
        Arguments : 
                    x_train : the set of training data samples
                    y_train : the set of training labels 
                    tuning : True when hyperparameters search is performed and False when not
                    search : 'random' for Random search or 'grid' for Grid Search
        """
        if tuning:
            if search == 'grid':
                #performs the hyperparameters search with Grid Search
                search_method = GridSearchCV(self.classifier_model, self.parameters, cv=10)
            elif search == 'random':
                #performs the hyperparameters search with Random Search
                search_method = RandomizedSearchCV(self.classifier_model, self.parameters, cv=10)
            
            #train the returned classifier with the best parameters, it's refit the classifier 
            clf = search_method.fit(x_train, y_train)
            score_training = clf.score(x_train, y_train)
            
            #the classifier with the best hyperparameters values is returned and will be used for test
            self.classifier_model = clf.best_estimator_

        else:
            #train the classifier
            clf = self.classifier_model.fit(x_train, y_train)
            score_training = self.classifier_model.score(x_train, y_train)

        return clf, score_training

    def predict_classifier(self, x_test, y_test):
        """
        this function returns the prediction of the test data after the classifier was trained and the score of the prediction
        Arguments : 
                    x_test : test data set (features of test data)
                    y_test : the labels of the data test set
        """
        #prediction on the test data
        out_predictions = self.classifier_model.predict(x_test)
        #the score or accuracy of test data
        score_prediction = self.classifier_model.score(x_test, y_test)
        
        return out_predictions, score_prediction
