import numpy as np
import os
import sklearn
import pandas as pd
from sklearn import preprocessing
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split, StratifiedShuffleSplit
from sklearn.preprocessing import LabelEncoder, RobustScaler, StandardScaler


def load_data(path_to_data, split_method='simple', standardize=True, data_transform=None, n_pca=10):
    """
    This functions returns the training and test sets that will be the input of the classifiers
    arguments : 
        path_to_data : the path/name of the folder containing the train test data
        split_method : 'simple' or 'stratified'
                       'simple' for "train_test_split" method from sklearn
                       'stratified' for "StratifiedShuffleSplit method"
                       by default 'simple'
        standardize : True or False, default False
        data_transform : with values in 'PCA', 'no_outliers_scale', 
                         when 'no_outliers_scale' standardize is set to False
        n_pca: int, the number of principle components, default is 10 
    returns : 
            the train and test set of data and labels
    """
    #directory where the data is located
    data_directory = os.path.join(os.getcwd(), path_to_data)
    #read the train.csv file containing the training data
    data = pd.read_csv(os.path.join(data_directory, 'train.csv'))

    features_data = np.asarray(data[data.columns[2:194]])
    #
    labels = data['species']
    labels_unique = labels.unique()
    n_classes = len(labels)
    #print(labels_unique, 'the number of labels', len(labels_unique))
    lbl=LabelEncoder().fit(labels)
    labels = np.asarray(lbl.transform(labels))
    classes = list(lbl.classes_)

    #if set to simple, then cross-validation will be used in the research of hyperparameters
    if split_method == 'simple':
        data_train, data_test, label_train, label_test = train_test_split(features_data, labels, train_size=0.8)

    #used when balancing the classes for training and test set with cross-validation
    elif split_method == 'stratified':
        sss = StratifiedShuffleSplit(n_splits=10, test_size=0.2, random_state=256)
        for train_index, test_index in sss.split(features_data, labels):
            print("TRAIN:", train_index, "TEST:", test_index)
            data_train, data_test = features_data[train_index], features_data[test_index]
            label_train, label_test = labels[train_index], labels[test_index]
        
    print('the shape of train set :', data_train.shape)
    print('the shape of test set :', data_test.shape)

    #we normalize the training data and the test data if standardize is set to True
    if standardize:
        scaler = preprocessing.StandardScaler().fit(data_train)
        data_train =  scaler.transform(data_train)
        data_test = scaler.transform(data_test)
    
    #PCA if the pca method was called by setting the value of data_transform to 'PCA'
    if data_transform=='PCA':
        pca = PCA(n_components=n_pca)
        pca.fit(data_train)
        data_train = pca.fit_transform(data_train)
        data_test = pca.fit_transform(data_test)

    #use this method when you want to use robust scaling in such a way you can detect and eliminate outliers
    elif data_transform=='no_outliers_scale':
        transformer = RobustScaler().fit(data_train)
        data_train = transformer.transform(data_train)
        data_test = transformer.transform(data_test)

    return data_train, data_test, label_train, label_test, n_classes, classes
