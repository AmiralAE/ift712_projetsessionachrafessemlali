from model_training import SupervisedClassifiers
from dataloader import load_data
import pandas as pd
import os 
import seaborn as sb
import matplotlib.pyplot as plt


def main(compare=False):
    """
        This is the main function of the script to run
        Argument : bool True or False
                    True if you ant to train all classifiers and compare them
                    False when you want to train just on classifier
    """
    #we load train and test data from the folder dataset
    data_train, data_test, label_train, label_test, n_classes, labels_unique = load_data('dataset')

    #data directory
    data_dir =os.path.join(os.getcwd(), 'dataset')

    #data to test on for submission
    submit_test_data = pd.read_csv(os.path.join(data_dir, 'test.csv'))

    #get the column's ids, the ids will be used for the submission file
    submit_test_data_id = submit_test_data.id

    #drop the id's columns for and just keep the features
    submit_test_data = submit_test_data.drop(['id'], axis=1)

    #compare set to True, this will be used
    if compare:
        compare_results = pd.DataFrame(columns=['classifier', 'accuracy'])
        for classifier_name in ['SVM','LDA','DecisionTree','RandomForest','MLP','KNN']:
            clf = SupervisedClassifiers(method = classifier_name)
            train_clf, train_score = clf.train_classifier(data_train, label_train)
            test_clf, test_score = clf.predict_classifier(data_test, label_test)
            out_acc = pd.DataFrame([[classifier_name, round(test_score, 4)*100]], columns=['classifier', 'accuracy'])
            compare_results = compare_results.append(out_acc)
        print(compare_results)
        sb.set_color_codes("muted")
        sb.barplot(x='accuracy', y='classifier', data=compare_results, color='indigo')

        plt.xlabel('accuracy score %')
        plt.title('Accuracy of Classifier')
        plt.savefig('accuracy_compare_plot.png')

    #in general while we test a specific classifier, we use this
    else:
        #the classifier is selected along with the hyperparameter's dictionnary
        clf = SupervisedClassifiers(method = 'MLP' )
        #train the classifier with the hyperparameter's search
        train_clf, train_score = clf.train_classifier(data_train, label_train)
        #test the classifier with the best estimator (the hyperparameters that give the best classifier)
        test_clf, test_score = clf.predict_classifier(data_test, label_test)
        #predict the probabilities of test classes for the test data to be submitted
        results_test = train_clf.predict_proba(submit_test_data)
        print(results_test)
        #stack the results on pandas dataframe
        submission_dataFrame = pd.DataFrame(results_test, columns=labels_unique)
        print(submission_dataFrame)
        submission_dataFrame.insert(0, 'id', submit_test_data_id)
        submission_dataFrame.reset_index()
        #save the results on the csv file to be submitted
        submission_dataFrame.to_csv(os.path.join(data_dir,'sample_submission.csv'), index = False)
        submission_dataFrame.tail()
    
    #print the score of traininf process and test process
    print('the accuracy of training for the classifier is:{:.4%}'.format(train_score))
    print('the accuracy of predicitons for the classifier is: {:.4%}'.format(round(test_score, 2)))


if __name__ == "__main__":
    #change to True if you want to compare the classifiers
    main(compare=False)